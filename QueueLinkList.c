#include<stdio.h>   
#include<stdlib.h>  
struct node   
{  
    int data;   
    struct node *next;  
};  
struct node *front;  
struct node *rear; 
struct node *n,*temp;  
void insert();  
void del();  
void display();  
void main ()  
{  
    printf("***QUEUE USING LINKED LIST***\n\n"); 
    int choice;   
    while(choice != 4)   
    {     
        printf("\n1.insert\n2.Delete\n3.Display\n4.Exit");  
        printf("\nEnter your choice: ");  
        scanf("%d",& choice);  
        switch(choice)  
        {  
            case 1: insert();  
            break;  
            case 2: del();  
            break;  
            case 3: display();  
            break;  
            case 4: exit(0);  
            break;  
            default:   
            printf("Enter valid choice??");  
        }  
    }  
}  
void insert()  
{    
    int item;   
      
    n = (struct node *) malloc (sizeof(struct node));  
    if(n == NULL)  
    {  
        printf("\nOVERFLOW");  
        return;  
    }  
    else  
    {   
        printf("Enter data: ");  
        scanf("%d",&item);  
        n -> data = item;  
        if(front == NULL)  
        {  
            front = n;  
            rear = n;   
            front -> next = NULL;  
            rear -> next = NULL;  
        }  
        else   
        {  
            rear -> next = n;  
            rear = n;  
            rear->next = NULL;  
        }  
    }  
    printf("Item Inserted\n");
}     
void del ()  
{     
    if(front == NULL)  
    {  
        printf("\nUNDERFLOW\n");  
        return;  
    }  
    else   
    {  
        temp = front;  
        front = front -> next;
		printf("%d is deleted\n",temp->data);  
        free(temp);  
    }  
}  
void display()  
{      
    n = front;      
    if(front == NULL)  
    {  
        printf("\nEmpty queue\n");  
    }  
    else  
    {   
	    printf("Elements in queue are: ");  
        while(n != NULL)   
        {  
            printf("%d\t\n",n -> data);  
            n = n -> next;  
        }  
    }  
}  
