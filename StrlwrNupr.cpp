//string in lower case
#include<stdio.h>
#include<string.h>
int main()
{
	printf("****STRING IN LOWER CASE****\n");
	char str[30]="NARUTO";
	strlwr(str);
	printf("STRING IN LOWER CASE IS %s\n\n",str);
	printf("****STRING IN UPPER CASE****\n");
	char str1[10]="uzumaki";
	strupr(str1);
	printf("String in upper case is %s",str1);
}
