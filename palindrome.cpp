//wap to check given number is palindrome or not
#include<stdio.h>
main()
{
	int n,reverse = 0,rem,temp;
    printf("Enter an integer:\n");
    scanf("%d",&n);
    temp=n;

    while(n != 0) 
	{
        rem = n % 10;
        reverse=reverse*10+rem;
        n/=10;
    }
    if (temp==reverse )
    {
        printf("%d is a palindrome.",temp);
    }
    else
    {
        printf("%d is not a palindrome.",temp);
    }
	
}
