//wap to find roots of quadratic equation
#include<stdio.h>
#define sqrt
int main()
{
	float a,b,c,d;  // a=coeff of x^2,b=coeff of x,c=constant
	float x,y;
	printf("ENTER THE VALUES");
	scanf("%f %f %f",&a,&b,&c);   // values of coefficients
	
	d= b*b-4*a*c;
	if (d<0)
	{
		printf("THE ROOTS ARE IMAGINARY");
	}
	if(d==0)
	{
		printf("THE ROOTS ARE EQUAL\n");
		x=-b/(2*a);
		y=-b/(2*a);
		printf("THE ROOTS ARE %f AND %f\n",x,y);
	}
    if(d>0)
	{
		printf("THE ROOTS ARE REAL NUMBERS\n");
		x=(-b+sqrt(d))/(2*a);
		y=(-b-sqrt(d))/(2*a);
		printf("THE ROOTS ARE %f and %f",x,y);
	}
}
