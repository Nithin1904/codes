// Program to implement arithmatic operators
#include <stdio.h>

int main()
{
	int a;
	int b;
	
    printf("Enter the values\n");
    scanf("%d %d",&a,&b);
    
    printf("The a+b value of %d and %d is %d\n",a,b,a+b);
    printf("The a-b value of %d and %d is %d\n",a,b,a-b);
    printf("The a*b value of %d and %d is %d\n",a,b,a*b);
    printf("The a/b value of %d and %d is %d\n",a,b,a/b);
    printf("The a%b value of %d and %d is %d\n",a,b,a%b);
}
