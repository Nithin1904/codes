# include<stdio.h>
# define MAX 5

void insert_rear();
void insert_front();
void delete_rear();
void delete_front();
void input_que();
void output_que();
void display();
int deque[MAX], front = -1,rear = -1;

void main()
{	
    int ch;
	printf("1.Input restricted dequeue\n");
	printf("2.Output restricted dequeue\n");
	printf("Enter your choice: ");
	scanf("%d",&ch);
	printf("\n");
	switch(ch)
	{
	 case 1 :
		input_que();
		break;
	 case 2:
		output_que();
		break;
	 default:
		printf("Wrong choice\n");
	}
}

void insert_rear()
{
	int item;
	if((front == 0 && rear == MAX-1) || (front == rear+1))
	{
		printf("Queue Overflow\n");
		return;
	}
	if (front == -1)  
	{
		front = 0;
		rear = 0;
	}
	else
	{
	    if(rear == MAX-1)  
		rear = 0;
	    else
		rear = rear+1;
	}
	printf("Input the element for adding in queue : ");
	scanf("%d", &item);
	deque[rear] = item ;
}

void insert_front()
{
	int item;
	if((front == 0 && rear == MAX-1) || (front == rear+1))
	{
		printf("Queue Overflow \n");
		return;
	}
	if (front == -1)
	{
		front = 0;
		rear = 0;	 
	}
	else
	{
	    if(front== 0)
		front=MAX-1;
	    else
		front=front-1;
	}
	printf("Input the element for adding in queue : ");
	scanf("%d", &item);
	deque[front] = item ;	 }

void delete_front()
{
	if (front == -1)
	{
		printf("Queue Underflow\n");
		return ;	
	}
	printf("Element deleted from queue is : %d\n",deque[front]);
	if(front == rear) 
	{
		front = -1;
		rear=-1;
	}
	else
	{
		if(front == MAX-1)
			front = 0;
		else
		front = front+1;
    }
}

void delete_rear()
{
    if (front == -1)
	{
	   printf("Queue Underflow\n");
		return ;	 
	}
	printf("Element deleted from queue is : %d\n",deque[rear]);
	if(front == rear) 
	{	
	    front = -1;
		rear=-1;
	}
	else
		if(rear == 0)
			rear=MAX-1;
		else
			rear=rear-1;	
}

void display()
{
	int front_pos = front,rear_pos = rear;
	if(front == -1)
	{
		printf("Queue is empty\n");
		return;
	}
	printf("Queue elements :\n");
	if( front_pos <= rear_pos )
	{
		while(front_pos <= rear_pos)
		{
			printf("%d ",deque[front_pos]);
			front_pos++;	
		}	
	}
	else
	{
		while(front_pos <= MAX-1)
		{
			printf("%d ",deque[front_pos]);
			front_pos++;	
		}
		front_pos = 0;
		while(front_pos <= rear_pos)
		{
			printf("%d ",deque[front_pos]);
			front_pos++;
		}
	}
	printf("\n");
}

void input_que()
{
	int ch;
	do
	{
		printf("1.Insert at rear\n");
		printf("2.Delete from front\n");
		printf("3.Delete from rear\n");
		printf("4.Display\n");
		printf("5.Quit\n");
		printf("Enter your choice: ");
		scanf("%d",&ch);

		switch(ch)
		{	case 1:
			insert_rear();
			break;
		 case 2:
			delete_front();
			break;
		 case 3:
			delete_rear();
			break;
		 case 4:
			display();
			break;
		 case 5:
            break;
		 default:
			printf("Wrong ch\n");
		}
	}while(ch!=5);
}

void output_que()
{
	int ch;
	do
	{
		printf("1.Insert at rear\n");
		printf("2.Insert at front\n");
		printf("3.Delete from front\n");
		printf("4.Display\n");
		printf("5.Quit\n");
		printf("Enter your ch : ");
		scanf("%d",&ch);
		switch(ch)
		{
		 case 1:
			insert_rear();
			break;
		 case 2:
			insert_front();
			break;
		 case 3:
			delete_front();
			break;
		 case 4:
			display();
			break;
		 case 5:
			break;
		 default:
			printf("Wrong choice\n");
		}
	}while(ch!=5);
}
