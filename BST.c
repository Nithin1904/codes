#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct node
{
	int data;
	struct node *left;
	struct node *right;
};
struct node *root=NULL,*n,*temp,*p,*t,*parent;
void insert();
void del();/*
void search();*/
void inorder(struct node*);
void main()
{
	int ch;
	printf("***BINARY SEARCH TREE***\n\n\n");
	while(ch!=5)
	{
		printf("MAIN MENU:\n");
		printf("1.Insert\n2.Delete\n3.Search\n4.Inorder traversal\n");
		printf("ENTER YOUR CHOICE:");
		scanf("%d",&ch);
		switch(ch)
		{
			case 1: insert();
			break;
			case 2: del();
			break;/*
			case 3: search()
			break;*/
			case 4: inorder(root);
			break;
			case 5: exit(1);
			break;
		}
		
	}
}

void insert()
{
	n=(struct node*)malloc(sizeof(struct node));
	printf("ENTER DATA:");
	scanf("%d",&n->data);
	n->left=NULL;
	n->right=NULL;
	if(root==NULL)
	{
		root=n;
	}
	else
	{
		temp=root;
		while(temp!=NULL)
		{
			if(n->data > temp->data)
			{
				if(temp->right==NULL)
				{
					temp->right=n;
					break;
				}
				else
				temp=temp->right;
			}
				else
				{
					if(temp->left==NULL)
					{
						temp->left=n;
						break;
					}
					else
					{
						temp=temp->left;
					}
				}
			}
		}
		printf("\n");
}
	
void del()
{
	int ele;
  if(root==NULL)
  {
	printf("NOT ELEMENTS TO DELETE\n");
  }
  else
  {
  	printf("ENTER ELEMENT TO DELETE:");
  	scanf("%d",&ele);
  	temp=root;
  	while(temp!=NULL)
  	{
  		if(ele < temp->data)
  		{
  			temp=temp->left;
  			p=temp;
		  }
		  else if(ele > temp->data)
		  {
		  	temp=temp->right;
		  	p=temp;
		  }
		  else if(ele == temp->data)
		  {
		  	if(temp->left==NULL && temp->right==NULL)
		  	{
		  		free(temp);
		  		break;
			  }
			  else if(temp->left!=NULL && temp->right==NULL)
			  {
			  	if(p->left==temp)
			  	{
			  		p->left=temp->left;
				  }
				  else
				  {
				  	p->right=temp->left;
				  }
				  free(temp);
				  break;
			}
			else if(temp->left==NULL && temp->right!=NULL)
			{
				if(p->left==temp)
				{
					p->right=temp->right;
				}
				else
				  {
				  	p->right=temp->right;
				  }
				  free(temp);
				  break;
			}
			else if(temp->left!=NULL && temp->right!=NULL)
			{
			    t=temp->left;
				while(t->right!=NULL)
				{
					parent=t;
					t=t->right;
					}
				temp->data=t->data;
				if(t->left!=NULL)
				{
					parent->right=t->left;
				}
				free(t);
				break;		
		    }			  
		  }
	  }
	  printf("\n");
  }
	printf("ELEMENT DELETED\n");
} 

void inorder(struct node *temp) 
{
  if (temp != NULL)
  {
    inorder(temp->left);
    printf("%d,", temp->data);
    inorder(temp->right);
    printf("\n");  
  }
}
