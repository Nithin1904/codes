// Chief and Street Food
#include<stdio.h>
int main()
{
	int t;
	scanf("%d",&t);
	while(t-->0)
	{
		int n,s,v,p,i,max=-1;
		scanf("%d",&n);
		for(i=0;i<n;i++)
		{
			scanf("%d %d %d",&s,&p,&v);
			if(v*(p/(s+1))>max)
			max=v*(p/(s+1));
		}
		printf("%d\n",max);
	}
	return 0;
}
